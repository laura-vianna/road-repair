﻿using RoadRepairInterfaces;

namespace RoadRepair
{
    public class RoadRepairItem
    {

        public Road Road { get; set; }
        public double MaterialToRepair { get; set; }

        public RoadRepairItem(Road road)
        {
            Road = road;
            MaterialToRepair = GetRepairVolume(road);
        }

        private static double GetRepairVolume(Road road)
        {
            var repair = RoadRepair.SelectRepairType(road);
            return repair.GetVolume();
        }
    }

    public static class RoadRepair
    {
        public static IRepairType SelectRepairType(Road road)
        {
            var density = road.GetDensity();

            // If the density of potholes is more than 20% the road should be resurfaced.
            if (density > 0.2)
            {
                return new Resurfacing(road);
            }

            // Otherwise it should be patched.
            return new PatchingRepair(road);
        }
    }
}
