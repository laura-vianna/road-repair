﻿using System;

namespace RoadRepair
{
    public class Road
    {
        public double Width { get; set; }
        public double Length { get; set; }
        public int Potholes { get; set; }

        /// <summary>
        /// Calculates the original area of a road as Width * Length 
        /// </summary>
        /// <returns></returns>
        public double GetArea()
        {
            return Width * Length;
        }

        /// <summary>
        /// Calculates the density of potholes based on the road's Width and Length
        /// </summary>
        /// <returns></returns>
        public double GetDensity()
        {
            return Potholes / GetArea();
        }
    }
}
