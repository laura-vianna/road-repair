using Microsoft.VisualStudio.TestTools.UnitTesting;
using RoadRepair;
using RoadRepairInterfaces;
using System.Collections.Generic;
using System.Linq;

namespace RoadRepairTests
{
    [TestClass]
    public class D_HelperTests
    {
        [TestMethod]
        [DataRow(1, 1, 1)]
        [DataRow(0, 1, 0)]
        [DataRow(3, 1.5, 4.5)]
        [DataRow(5, 2, 10)]
        public void CalculateRoadArea(double length, double width, double expected)
        {
            var road = new Road { Length = length, Width = width, Potholes = 0};

            var actual = road.GetArea();

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        [DataRow(1, 1, 0, 0)]
        [DataRow(1, 1, 1, 1)]
        [DataRow(5, 2.1, 2, 0.19)]
        [DataRow(5, 2, 2, 0.2)]
        [DataRow(5, 2, 3, 0.3)]
        public void CalculateDensity(double length, double width, int potholes, double expected)
        {
            var road = new Road { Length = length, Width = width, Potholes = potholes };

            var actual = road.GetDensity();

            Assert.AreEqual(expected, actual, 0.001);
        }


        [TestMethod]
        [DataRow(1, 1, 0, 0, typeof(PatchingRepair))]
        [DataRow(5, 2.1, 2, 0.19, typeof(PatchingRepair))]
        [DataRow(5, 2, 2, 0.2, typeof(PatchingRepair))]
        [DataRow(5, 2, 3, 0.3, typeof(Resurfacing))]
        [DataRow(1, 1, 1, 1, typeof(Resurfacing))]
        public void TestPlanner(double length, double width, int potholes, double density, System.Type expected)
        {
            var planner = new Planner();
            var road = new Road { Length = length, Width = width, Potholes = potholes };

            var repair = planner.SelectRepairType(road);

            Assert.IsInstanceOfType(repair, expected, $"The density was ${density}");
        }

        //make the planner work out which roads to repair, if there is not enough material available to repair all of them.
        [TestMethod]
        [DataRow(100, true)]
        [DataRow(1.30001, true)]
        [DataRow(1.29999, false)]
        [DataRow(1.25, false)]
        [DataRow(0, false)]
        public void TestAvailableMaterial(double availableMaterial, bool expected)
        {
            var road1 = new Road() { Length = 6, Width = 2, Potholes = 1 };
            var road2 = new Road() { Length = 3, Width = 2, Potholes = 5 };
            var road3 = new Road() { Length = 4, Width = 1.5, Potholes = 3 };
            var roads = new List<Road>() { road1, road2, road3 };
            var roadRepairInfo = new RoadRepairInfo(roads);

            var actual = roadRepairInfo.GetMaterialAvailability(availableMaterial);

            Assert.AreEqual(expected, actual);
        }

        //If the available material is less than the total volume needed to fix all
        [TestMethod]
        [DataRow(100, 3)]
        [DataRow(1.3, 3)]
        [DataRow(1.29, 2)]
        [DataRow(1.20, 1)]
        public void LimitedMaterialCount(double availableMaterial, int expected)
        {
            var planner = new Planner();
            var road1 = new Road() { Length = 6, Width = 2, Potholes = 1 };
            var road2 = new Road() { Length = 3, Width = 2, Potholes = 5 };
            var road3 = new Road() { Length = 4, Width = 1.5, Potholes = 3 };
            var roads = new List<Road>() { road1, road2, road3 };

            var actual = planner.SelectRoadsToRepair(roads, availableMaterial);

            Assert.AreEqual(expected, actual.Count);
        }

        [TestMethod]
        public void GetRoadRepairItem()
        {
            var road1 = new Road() { Length = 6, Width = 2, Potholes = 1 };
            var materialToRepair = 0.1;

            var actual = new RoadRepairItem(road1);

            Assert.AreEqual(road1, actual.Road);
            Assert.AreEqual(materialToRepair, actual.MaterialToRepair);
        }

        [TestMethod]
        public void GetRoadRepairInfo()
        {
            var road1 = new Road() { Length = 6, Width = 2, Potholes = 1 };
            var road2 = new Road() { Length = 3, Width = 2, Potholes = 5 };
            var road3 = new Road() { Length = 4, Width = 1.5, Potholes = 3 };
            var roads = new List<Road>() { road1, road2, road3 };
            var materialToRepair3 = 0.6;
            var totalMaterialToRepair = 1.3;

            var actual = new RoadRepairInfo(roads);

            Assert.AreEqual(road3, actual.RoadRepairList[2].Road);
            Assert.AreEqual(materialToRepair3, actual.RoadRepairList[2].MaterialToRepair, SimilarDoubleHandler.Delta);
            Assert.AreEqual(totalMaterialToRepair, actual.TotalMaterialToRepair, SimilarDoubleHandler.Delta);
        }

        [TestMethod]
        public void GetRoadRepairInfoByLeastPotholes()
        {
            var road1 = new Road() { Length = 6, Width = 2, Potholes = 1 };
            var road2 = new Road() { Length = 3, Width = 2, Potholes = 5 };
            var road3 = new Road() { Length = 4, Width = 1.5, Potholes = 3 };
            var roads = new List<Road>() { road1, road2, road3 };
            var roadRepairInfo =  new RoadRepairInfo(roads);

            var actual = roadRepairInfo.OrderByLeastPotholes();

            Assert.AreEqual(road2, actual.ElementAt(2).Road);
            Assert.AreEqual(road3, actual.ElementAt(1).Road);
            Assert.AreEqual(road1, actual.ElementAt(0).Road);
        }

        [TestMethod]
        public void GetReparableRoads()
        {
            var road1 = new Road() { Length = 6, Width = 2, Potholes = 1 };
            var road2 = new Road() { Length = 3, Width = 2, Potholes = 5 };
            var road3 = new Road() { Length = 4, Width = 1.5, Potholes = 3 };
            var roads = new List<Road>() { road1, road2, road3 };
            var roadRepairInfo = new RoadRepairInfo(roads);
            var availableMaterial = 1.29;

            var actual = roadRepairInfo.GetReparableRoads(availableMaterial);

            Assert.AreEqual(road3, actual[0].Road);
            Assert.AreEqual(road2, actual[1].Road);
        }
    }
}
